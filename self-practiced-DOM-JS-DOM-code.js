let form = document.getElementById('addInForm');
let list = document.getElementById('items-display');
let filter = document.getElementById('filter-bar');


let storedData = localStorage.getItem('todo');
let locValues = JSON.parse(storedData)

locValues.forEach((itm) => 
{
    let list_item  = document.createElement('li');
    list_item.className = 'item';
    list_item.appendChild(document.createTextNode(itm));

    let delBtn = document.createElement('button');
    delBtn.className = 'del-button remove'
    delBtn.appendChild(document.createTextNode('X'));
    list_item.appendChild(delBtn);

    list.appendChild(list_item);
})


form.addEventListener('submit', addnewItem);
function addnewItem(element){

    element.preventDefault();

    let itemVal = document.getElementById('add-item-box').value;
    if(itemVal !== "")
    {       
        let local_store = localStorage.getItem('todo');
        if(local_store)
        {
            dataFromStore = JSON.parse(local_store);
            let itemsValue = dataFromStore.reduce((acc, curr) => {
                acc.push(curr);
                return acc;
            },[])
            if(!itemsValue.includes(itemVal))
            {
                dataFromStore.push(itemVal);
                localStorage.setItem('todo', JSON.stringify(dataFromStore));
            }
            else{
                alert('Item already present');
            }
        }
        else
        {
            let dataFromStore = [];
            dataFromStore.push(itemVal);
            localStorage.setItem('todo', JSON.stringify(dataFromStore));
        }

        local_store = localStorage.getItem('todo');
        let localValues = JSON.parse(local_store);

        listTagElements = document.querySelectorAll('li');
        if(listTagElements.length > 0)
        {
            listTagElements.forEach((entry) => {
                entry.remove();
            })
        }

        localValues.forEach((itm) => 
        {
            let list_item  = document.createElement('li');
            list_item.className = 'item';
            list_item.appendChild(document.createTextNode(itm));
    
            let delBtn = document.createElement('button');
            delBtn.className = 'del-button remove'
            delBtn.appendChild(document.createTextNode('X'));
            list_item.appendChild(delBtn);
    
            list.appendChild(list_item);
        })

        document.getElementById('add-item-box').value = "";
    }
}


list.addEventListener('click', deleteItem)
function deleteItem(element){
    if(element.target.classList.contains('remove')){
        if(confirm("You want to remove this entry?")){

            let local_store = (localStorage.getItem('todo'));
            let localValues = JSON.parse(local_store);
            
            let item_text = element.target.parentElement.textContent;
            item_text = item_text.substring(0,item_text.length-1)
            let updatedList = localValues.reduce((acc, val) => {
                if(val !== item_text)
                {
                    acc.push(val);
                }
                return acc;
            },[])
            console.log(updatedList)
            localStorage.setItem('todo', JSON.stringify(updatedList));
            var list_item = element.target.parentElement;
            list.removeChild(list_item);
        }
    }
}


filter.addEventListener('keyup', filterItems)
function filterItems(element){

    let searchedItem = element.target.value.toLowerCase();
    let items = list.getElementsByTagName('li');
        Array.from(items).forEach((item) => {
        let itemName = item.firstChild.textContent;

        if(itemName.toLowerCase().indexOf(searchedItem) != -1){
            item.style.display = 'flex';
        }
        else
        {
            item.style.display = 'none';
        }
    })
}